# PKVPNManager

支持IPSec、IKEv2协议 二步启动

## 描述

鉴于Apple自带的VPN开发过于复杂难以理解，重新封装了一套VPN开发框架，对于简单的VPN开发只需要配置，和启动。现开源给大家，欢迎提交Issues，如果觉得不错的话一定不要忘了star和fork哦。相关技术问题欢迎加入QQ交流群（群号：92073722）或查看[我的博客](pkwans.github.io)

## Usage 使用方法

* 在开发者网站，新增一个 AppID，并开启 Network Extensions 和 Personal VPN 的服务
* 修改 AppID 为已经开启上述两个服务的 AppID
* 在工程的 capabilities 中开启 Personal VPN
* 为当前工程，导入 NetworkExtension.framework 和 Security.framework
* 将子文件夹PKVPNManager拖入到项目中。
* 导入MPVPNManager.h文件 

```objc
    #import "PKVPNManager.h"
```

* 初始化

```objc
	[PKVPNManager shareInstance];
```

* 检查VPN是否配置

```objc

    [[PKVPNManager shareInstance] checkVPNConfigCompleteHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"success:%d error:%@",success,error.localizedDescription);
    }];
    
```

* 配置VPN参数

```objc

    [[PKVPNManager shareInstance] settingUpVPNConfig:config completeHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"save IKEv2 config %@ errot:%@",success?@"success":@"fail",error);
    }];
    
```

* 移除VPN配置信息

```objc

    [[PKVPNManager shareInstance] removeVPNConfigCompleteHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
    
```

* 检测IP是否是你代理设置的IP (VPN 是否为当前VPN)

```objc

    [[PKVPNManager shareInstance] checkVPNISCurrentOpenCompleteHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"checkVPNISCurrentOpenCompleteHandler:%d error:%@",success,error);
    }];
    
```

* 监测VPN状态的改变

```objc
	[[PKVPNManager shareInstance] startMonitoringVPNStatusDidChange:^(enum NEVPNStatus status) {
        [self updateTitle];
        NSLog(@"status change:%ld",(long)status);
        // 在这里知道VPN是否开启
        // 检测IP是否是你代理设置的IP
        
        NSLog(@"currentVPNConnectConfig:%@",[PKVPNManager shareInstance].currentVPNConnectConfig);
    }];
    
```
* 其它参数详解

```objc

	//是否存储VPN config到本地
	@property (nonatomic, assign) BOOL autoStorageConfig;
	//当前VPN连接类型
	@property (nonatomic, assign, readonly) PKVPNConnectType currentVPNConnectType;
	// 当前VPN状态
	@property (nonatomic, assign, readonly) NEVPNStatus currentVPNStatus;
	// 当前VPN连接的配置信息
	@property (nonatomic, strong, readonly, nullable) NSDictionary *currentVPNConnectConfig;

```

* IPSec协议，共享秘钥方式

```objc

    //设置VPN配置信息
    //共享秘钥方式
    PKVPNConfigInfo *config = [PKVPNConfigInfo new];
    config.configTitle = @"PKVPNManager";
    config.VPNConnectType = PKVPNConnectTypeIPSec;
    config.serverAddress = @"xxx.xxx.xxx.xxx";
    config.username = @"username";
    config.password = @"password";
    config.sharePrivateKey = @"sharePrivateKey";
    [[PKVPNManager shareInstance] settingUpVPNConfig:config completeHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"save IPSec config %@ error:%@",success?@"success":@"fail",error);
    }
     ];
     
```

* IKEv2协议

```objc

    /**如果是证书验证需要安装pem描述文件*/
    /**不需要验证信息方式*/
    /**直接用AirDrop将CACert.pem发送到手机即可*/
    PKVPNConfigInfo *config = [PKVPNConfigInfo new];
    config.configTitle = @"PKVPNManager";
    config.VPNConnectType = PKVPNConnectTypeIKEv2;
    config.serverAddress = @"xxx.xxx.xxx.xxx";
    config.username = @"username";
    config.password = @"password";
    config.remoteID = @"remoteID";
    config.serverCertificateCommonName = @"StrongSwan Root CA";
    config.serverCertificateIssuerCommonName = @"StrongSwan Root CA";
    [[PKVPNManager shareInstance] settingUpVPNConfig:config completeHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"save IKEv2 config %@ errot:%@",success?@"success":@"fail",error);
    }];
    
```

* 开启VPN

```objc
	    [[PKVPNManager shareInstance] startVPNConnectCompletionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
```

* 关闭VPN

```objc
    [[PKVPNManager shareInstance] stopVPNConnectCompletionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
```


## 注意事项
* 如果提示vpn服务器并未响应是配置账号的问题 请优先确保账号正确型，可在macOS或者iOS系统自带的VPN测试

## 服务端配置参考
* 服务端可以用 [strongswan](https://www.strongswan.org/)
* 服务端配置 [https://raymii.org/s/tags/vpn.html](https://raymii.org/s/tags/vpn.html)

## 联系方式:
* QQ : 351420450
* Email : pkwans@qq.com
* 更多相关问题欢迎加入QQ交流群（群号：92073722）
* 另可商务合作支持：iOS、Android、Windows、macOS，具体请加QQ详谈

## 特别鸣谢:
* 您的star的我开发的动力，如果代码对您有帮助麻烦动动您的小手指点个start。

