//
//  PKVPNManager.h
//  PKVPNManagerSample
//
//  Created by pkwans on 2017/7/7.
//  Copyright © 2017年 pkwans. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NetworkExtension/NetworkExtension.h>

typedef void(^PKVPNManagerCompletionHandler)(BOOL success , NSError * _Nullable error);
typedef void(^PKVPNManagerVPNStatusDidChange)(enum NEVPNStatus status);

/*!
 * @typedef PKVPNConnectType
 * @abstract VPN connect tpey codes
 */
typedef NS_ENUM(NSInteger, PKVPNConnectType){
    PKVPNConnectTypeNone = 0,
    PKVPNConnectTypeIPSec = 1,
    PKVPNConnectTypeIKEv2 = 2,
};


@interface PKVPNConfigInfo : NSObject

@property (nonatomic, assign) PKVPNConnectType VPNConnectType;
/** VPN标题 */
@property (nonatomic, copy, nonnull) NSString *configTitle;
/** 服务器地址 */
@property (nonatomic, copy, nonnull) NSString *serverAddress;
/** 用户名 */
@property (nonatomic, copy, nullable) NSString *username;
/** 密码 */
@property (nonatomic, copy, nullable) NSString *password;
/** vpn验证证书 p12文件  PKCS12 格式 当前属性与sharePrivateKey 仅有一个有效默认为sharePrivateKey优先*/
@property (nonatomic, copy, nullable) NSData *identityData;
/** 证书秘钥 */
@property (nonatomic, copy, nullable) NSString *identityDataPassword;
/** 共享秘钥 */
@property (nonatomic, copy, nullable) NSString *sharePrivateKey;
/** 本地ID */
@property (nonatomic, copy, nullable) NSString *localID;
/** remoteID */
@property (nonatomic, copy, nullable) NSString *remoteID;

/** IKEv2 */
@property (nonatomic, copy, nullable) NSString *serverCertificateIssuerCommonName;

@property (nonatomic, copy, nullable) NSString *serverCertificateCommonName;

@end

/*!
 * @interface PKVPNManager
 * @discussion The PKVPNManager class declares the programmatic interface for an VPN manager.
 *
 * Instances of this class are thread safe.
 */
NS_CLASS_AVAILABLE(10_11, 8_0)
@interface PKVPNManager : NSObject

#pragma mark - main

+ (instancetype _Nonnull )shareInstance;

/*!
 * @method checkVPNConfigCompleteHandler:
 * @discussion This function is used to check VPN config is installed.
 * @param completion A block that success or error.
 */
- (void)checkVPNConfigCompleteHandler:(PKVPNManagerCompletionHandler _Nonnull )completion;

/*!
 * @method settingUpVPNConfig:completeHandler:
 * @discussion This function is used to create or update VPN config @see PKVPNConfigInfo.
 * @param config A VPN config @see PKVPNConfigInfo
 * @param completion A block that success or error.
 */
- (void)settingUpVPNConfig:(PKVPNConfigInfo * _Nonnull)config
           completeHandler:(PKVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method removeVPNConfigCompleteHandler:
 * @discussion This function is used to remove VPN config.
 * @param completion A block that success or error.
 */
- (void)removeVPNConfigCompleteHandler:(PKVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method startVPNConnectCompletionHandler:
 * @discussion This function is used to start VPN connect.
 * @param completion A block that success or error.
 */
- (void)startVPNConnectCompletionHandler:(PKVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method stopVPNConnectCompletionHandler:
 * @discussion This function is used to stop VPN connect.
 * @param completion A block that success or error.
 */
- (void)stopVPNConnectCompletionHandler:(PKVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method startMonitoringVPNStatusDidChange:
 * @discussion This function is used to star monitoring VPN status @see NEVPNStatu.
 * @param VPNStatusDidChange A block that  VPN status did change.
 */
- (void)startMonitoringVPNStatusDidChange:(PKVPNManagerVPNStatusDidChange _Nullable )VPNStatusDidChange;

/*!
 * @method checkVPNISCurrentOpenCompleteHandler:
 * @discussion This function is used to check VPN is not current App open.
 * @param completion A block that success or error.
 */
- (void)checkVPNISCurrentOpenCompleteHandler:(PKVPNManagerCompletionHandler _Nonnull )completion;

#pragma mark - property

/*!
 * @property autoStorageConfig
 * @discussion when create or update VPN sucessful, is not auto storage config in UserDefaults.
 */
@property (nonatomic, assign) BOOL autoStorageConfig;

/*!
 * @property currentVPNConnectType
 * @discussion current VPN connect type @see PKVPNConnectType.
 */
@property (nonatomic, assign, readonly) PKVPNConnectType currentVPNConnectType;

/*!
 * @property currentVPNStatus
 * @discussion current VPN status @see NEVPNStatus.
 */
@property (nonatomic, assign, readonly) NEVPNStatus currentVPNStatus;

/*!
 * @property currentVPNConnectConfig
 * @discussion current VPN connect config 
 */
@property (nonatomic, strong, readonly, nullable) NSDictionary *currentVPNConnectConfig;

#pragma mark - helper

/*!
 * @method setGlobalServiceName:
 * @discussion set serviceName default bundleIdentifier
 */
+ (void)setGlobalServiceName:(NSString *_Nonnull)serviceName;

/*!
 * @method setAppGroupsName:
 * @discussion if your app have widget or other extension about AppGroups please set this .
 * ex:[PKVPNManager setAppGroupsName:@"group.com.pkwans.Vpn"];
 * if your app not have other , set this nil .
 */
+ (void)setAppGroupsName:(NSString *_Nullable)appGroupsName;

/*!
 * @method getConfig:
 * @return VPN config to @see PKVPNConfigInfo
 */
+ (id _Nullable )getConfig;

@end

