//
//  PKVPNManager.h
//  PKVPNManagerSample
//
//  Created by pkwans on 2017/7/7.
//  Copyright © 2017年 pkwans. All rights reserved.
//


#import "PKVPNManager.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <MJExtension/MJExtension.h>


#ifdef DEBUG
#define Log(format, ...) NSLog((@"[file:%s]" "[func:%s]" "[line:%d]" format), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define Log(...);
#endif


@implementation PKVPNConfigInfo
MJCodingImplementation
@end

@interface PKVPNManager ()
@property (nonatomic, strong) NEVPNManager * vpnManager;
/** config info */
@property (nonatomic, readonly, strong) PKVPNConfigInfo * _Nullable config;
@end

@implementation PKVPNManager

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.vpnManager = [NEVPNManager sharedManager];
        [self performSelector:@selector(registerNetWorkReachability) withObject:nil afterDelay:0.35f];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) createKeychainPassword:(NSString *)password privateKey:(NSString *)privateKey
{
    if (password.length) {
         [[self class] createKeychainValue:password forIdentifier:PKVPNPasswordIdentifier];
    }
   
    if (privateKey.length) {
        [[self class] createKeychainValue:privateKey forIdentifier:PKVPNSharePrivateKeyIdentifier];
    }
   
}

- (void)checkVPNConfigCompleteHandler:(PKVPNManagerCompletionHandler _Nonnull )completion {
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            Log(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            if ([[NSString stringWithFormat:@"%@", _vpnManager.protocol] rangeOfString:@"persistentReference"].location != NSNotFound) {
                completion ? completion(YES, nil) : 0;
            } else {
                // 不存在
                completion ? completion(NO, [NSError errorWithDomain:@"no installed vpn config" code:0 userInfo:nil]) : 0;
            }
        }
    }];
}

- (void)settingUpVPNConfig:(PKVPNConfigInfo * _Nonnull)config
           completeHandler:(PKVPNManagerCompletionHandler _Nullable )completion {
    
    if (config == nil) {
        completion ? completion(NO,[NSError errorWithDomain:@"Configuration parameters cannot be empty" code:0 userInfo:nil]) : 0 ;
        return;
    }
    _config = config;
    
    [self createKeychainPassword:config.password privateKey:config.sharePrivateKey];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            completion ? completion(NO, error) : 0;
            return;
        }
        
        if (_vpnManager.connection.status == NEVPNStatusConnected) {
            completion ? completion(NO, [NSError errorWithDomain:@"VPN is connected" code:0 userInfo:nil]) : 0;
            return;
        }
        
         NEVPNProtocol *p = [self createVPNProtocol:config];
        _vpnManager.protocol = p;
        _vpnManager.onDemandEnabled = YES;
        _vpnManager.localizedDescription = _config.configTitle;
        _vpnManager.enabled = YES;
        // 保存设置
        [_vpnManager saveToPreferencesWithCompletionHandler:^(NSError *error) {
            if(error) {
                completion ? completion(NO, error) : 0;
            }
            else {
                if (_autoStorageConfig) {
                    [[self class] saveConfig:_config];
                }
                //                            id conf = [PKCommon getConfig];
//                [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
//                    if (error) {
//                        completion ? completion(NO, error) : 0 ;
//                        return;
//                    }
//                    else {
                        completion ? completion(YES, nil) : 0;
//                    }
//                }];
            }
        }];
        
    }];
}

- (void)removeVPNConfigCompleteHandler:(PKVPNManagerCompletionHandler _Nullable )completion {
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            Log(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            [_vpnManager removeFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    Log(@"删除 VPN 偏好设置失败 : %@", error);
                    completion ? completion(NO, error) : 0;
                } else {
                    _vpnManager.protocol = nil;
                    completion ? completion(YES, nil) : 0;
                }
            }];
        }
    }];
}

- (NEVPNProtocol *)createVPNProtocol:(PKVPNConfigInfo *)config {
    switch (config.VPNConnectType) {
            
        case PKVPNConnectTypeIPSec:
        {
            NEVPNProtocolIPSec *p = [NEVPNProtocolIPSec new];
            
            p.username = config.username;
            p.serverAddress = config.serverAddress;
            p.passwordReference = [[self class] searchKeychainCopyMatching:PKVPNPasswordIdentifier];
            
            if (
                [[self class] searchKeychainCopyMatching:PKVPNSharePrivateKeyIdentifier] &&
                config.sharePrivateKey) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
                p.sharedSecretReference = [[self class] searchKeychainCopyMatching:PKVPNSharePrivateKeyIdentifier];
            }
            else if (config.identityData && config.password) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
                p.identityData = config.identityData;
                p.identityDataPassword = config.identityDataPassword;
            }
            else{
                p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
            }
            p.localIdentifier = config.localID;
            p.remoteIdentifier = config.remoteID;
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            return p;
        }
            break;
        case PKVPNConnectTypeIKEv2:
        {
            NEVPNProtocolIKEv2 *p = [NEVPNProtocolIKEv2 new];
            p.username = config.username;
            p.passwordReference = [[self class] searchKeychainCopyMatching:PKVPNPasswordIdentifier];
            
            p.serverAddress = config.serverAddress;
            p.serverCertificateIssuerCommonName = config.serverCertificateCommonName;
            p.serverCertificateCommonName = config.serverCertificateCommonName;
            
            if (
                [[self class] searchKeychainCopyMatching:PKVPNSharePrivateKeyIdentifier] &&
                config.sharePrivateKey) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
                p.sharedSecretReference = [[self class] searchKeychainCopyMatching:PKVPNSharePrivateKeyIdentifier];
            }
            else if (config.identityData && config.password) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
                p.identityData = config.identityData;
                p.identityDataPassword = config.identityDataPassword;
            }
            else{
                p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
            }
            
            //                    p.identityData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"point-to-client2" ofType:@"p12"]];
            //                    p.identityDataPassword = @"vpnuser";
            
            p.localIdentifier = config.localID;
            p.remoteIdentifier = config.remoteID;
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            //                    NEEvaluateConnectionRule * ru = [[NEEvaluateConnectionRule alloc]
            //                                                     initWithMatchDomains:@[@"google.com"]
            //                                                     andAction:NEEvaluateConnectionRuleActionConnectIfNeeded];
            //
            //                    ru.probeURL = [[NSURL alloc] initWithString:@"http://www.google.com"];
            //
            //                    NEOnDemandRuleEvaluateConnection *ec =[[NEOnDemandRuleEvaluateConnection alloc] init];
            //                    //                ec.interfaceTypeMatch = NEOnDemandRuleInterfaceTypeWiFi;
            //                    [ec setConnectionRules:@[ru]];
            //                    [_vpnManager setOnDemandRules:@[ec]];
            
            
            return p;
        }
            break;
        case PKVPNConnectTypeNone:
            
            break;
    }
    return nil;
}

#pragma mark - 自动重连 BEGIN
- (void)registerNetWorkReachability{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetWork) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
}
/**
 *  检测网络
 */
-(void)checkNetWork{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN ||
            status == AFNetworkReachabilityStatusReachableViaWiFi) {
            if (self.vpnManager.connection.status == NEVPNStatusDisconnected) {
                [self startVPNConnectCompletionHandler:^(BOOL success, NSError * _Nullable error) {
                    
                }];
            }
        }
    }];
}

- (void)startVPNConnectCompletionHandler:(PKVPNManagerCompletionHandler)completion {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            Log(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            NSError *returnError;
            [_vpnManager.connection startVPNTunnelAndReturnError:&returnError];
            if (returnError) {
                Log(@"启动 VPN 失败 : %@", returnError);
              completion ? completion(NO, error) : 0;
            } else {
                completion ? completion(YES, nil) : 0;
            }
        }
    }];
}

- (void)stopVPNConnectCompletionHandler:(PKVPNManagerCompletionHandler)completion {
    
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            Log(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            [_vpnManager.connection stopVPNTunnel];
            completion ? completion(YES, nil) : 0;
        }
    }];
}



- (void)startMonitoringVPNStatusDidChange:(PKVPNManagerVPNStatusDidChange)VPNStatusDidChange
{
    [[NSNotificationCenter defaultCenter] addObserverForName:NEVPNStatusDidChangeNotification
                                                      object:self.vpnManager.connection
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      VPNStatusDidChange ? VPNStatusDidChange(self.vpnManager.connection.status) : 0;
                                                  }];
}

- (void)checkVPNISCurrentOpenCompleteHandler:(PKVPNManagerCompletionHandler _Nonnull )completion {
    NSError *error;
    NSURL *ipURL = [NSURL URLWithString:@"http://ipof.in/txt"];
    NSString *ip = [NSString stringWithContentsOfURL:ipURL encoding:NSUTF8StringEncoding error:&error];
    if ([ip isEqualToString:_config.serverAddress]) {
        completion ? completion (YES, nil) : 0;
        return;
    }
    completion ? completion (NO, error) : 0;
}


#pragma mark - gettter 

- (PKVPNConnectType)currentVPNConnectType {
    NSString *type = (NSString *)[[self currentVPNConnectConfig] objectForKey:@"type"];
    if (!type.length) {
        return PKVPNConnectTypeNone;
    }
    if ([type isEqualToString:@"ikev1"]) {
        return PKVPNConnectTypeIPSec;
    }
    return PKVPNConnectTypeIKEv2;
}

- (NEVPNStatus)currentVPNStatus {
    return _vpnManager.connection.status;
}

- (NSDictionary *)currentVPNConnectConfig {
    return (NSDictionary *)_vpnManager.protocolConfiguration;
}


 #pragma mark - Helper

static NSString *PKServiceName ;
static NSString *PKAppGroupsName;
NSString * const kMPVpnConfigKey = @"kPKVpnConfigKey";

+ (void)initialize {
    PKServiceName = [self getServiceName];
#warning if your app have widget or other extension about AppGroups please set this ，Otherwise shielding the following line.
    [self setAppGroupsName:@"group.com.pkwans.Vpn"];
}

+ (void)setGlobalServiceName:(NSString *_Nonnull)serviceName {
    PKServiceName = serviceName;
}

+ (void)setAppGroupsName:(NSString *)appGroupsName {
    PKAppGroupsName = appGroupsName;
}

+ (NSUserDefaults *)sharedUserDefaults {
    if (PKAppGroupsName.length) {
        return [[NSUserDefaults alloc] initWithSuiteName:PKAppGroupsName];
    }
    
    return [NSUserDefaults standardUserDefaults];
}

+ (BOOL)saveConfig:(id)config {
    NSUserDefaults *userDefaults = [self sharedUserDefaults];
    NSData * data  = [NSKeyedArchiver archivedDataWithRootObject:config];
    [userDefaults setObject:data forKey:kMPVpnConfigKey];
    return [userDefaults synchronize];
}

+ (id)getConfig {
    NSUserDefaults *userDefaults = [self sharedUserDefaults];
    if ([userDefaults objectForKey:kMPVpnConfigKey]) {
        NSData *data = [userDefaults objectForKey:kMPVpnConfigKey];
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return nil;
}


#pragma mark - KeyChain BEGIN

NSString * const PKVPNPasswordIdentifier = @"PKVPNPasswordIdentifier";
NSString * const PKVPNSharePrivateKeyIdentifier = @"PKVPNSharePrivateKeyIdentifier";

+ (NSString *)getServiceName {
    return [[NSBundle mainBundle] bundleIdentifier];
}

+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:[self getServiceName] forKey:(__bridge id)kSecAttrService];
    
    return searchDictionary;
}

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [searchDictionary setObject:@YES forKey:(__bridge id)kSecReturnPersistentRef];
    
    CFTypeRef result = NULL;
    SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, &result);
    
    return (__bridge_transfer NSData *)result;
}

+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];
    
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)dictionary);
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(__bridge id)kSecValueData];
    
    status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}
#pragma mark - KeyChain END




#define kKeychainServiceID @"com.CoderGin.VPNDemo.keychain.library"

// got from: http://useyourloaf.com/blog/2010/03/29/simple-iphone-keychain-access.html

- (NSMutableDictionary *)buildDefaultDictionaryForIdentity:(NSString*)identifier {
    
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    searchDictionary[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    searchDictionary[(__bridge id)kSecAttrGeneric] = encodedIdentifier;
    searchDictionary[(__bridge id)kSecAttrAccount] = encodedIdentifier;
    searchDictionary[(__bridge id)kSecAttrService] = kKeychainServiceID;
    
    return searchDictionary;
}

// 根据 identifier 获取钥匙串中的数据
- (NSData *)getDataInKeychainFromIdentifier:(NSString *)identifier returnReference:(BOOL)referenceOnly {
    
    // get default dictionary
    NSMutableDictionary *dict = [self buildDefaultDictionaryForIdentity:identifier];
    
    // set for searching
    dict[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    
    // need reference
    if (referenceOnly) {
        dict[(__bridge id)kSecReturnPersistentRef] = @YES;
    } else {
        dict[(__bridge id)kSecReturnData] = @YES;
    }
    
    // create result object
    CFTypeRef result = NULL;
    
    // Get result
    SecItemCopyMatching((__bridge CFDictionaryRef)dict, &result);
    
    // return result
    return (__bridge_transfer NSData *)result;
}

// 根据 identifier 获取钥匙串中的字符串数据
- (NSString*)getStringInKeychainFromIdentifier:(NSString*)identifier {
    
    NSData *keychainData = [self getDataInKeychainFromIdentifier:identifier returnReference:NO];
    return [[NSString alloc] initWithData:keychainData encoding:NSUTF8StringEncoding];
}

// 根据 identifier 获取钥匙串中的二进制数据
- (NSData *)getDataReferenceInKeychainFromIdentifier:(NSString *)identifier {
    
    return [self getDataInKeychainFromIdentifier:identifier returnReference:YES];
}

/// 设置钥匙串中的数据
- (BOOL)setKeychainWithString:(NSString*)string forIdentifier:(NSString*)identifier {
    
    NSMutableDictionary *searchDictionary = [self buildDefaultDictionaryForIdentity:identifier];
    NSData *keychainValue = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    if ([self getDataReferenceInKeychainFromIdentifier:identifier] == nil) {
        [searchDictionary setObject:keychainValue forKey:(__bridge id)kSecValueData];
        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)searchDictionary, NULL);
        if (status == errSecSuccess) {
            return YES;
        } else {
            return NO;
        }
    } else {
        NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];
        [updateDictionary setObject:keychainValue forKey:(__bridge id)kSecValueData];
        OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)searchDictionary, (__bridge CFDictionaryRef)updateDictionary);
        if (status == errSecSuccess) {
            return YES;
        } else {
            return NO;
        }
    }
}

@end
