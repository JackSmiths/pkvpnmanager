//
//  AppDelegate.h
//  PKVPNManagerSample
//
//  Created by pkwans on 2017/7/7.
//  Copyright © 2017年 pkwans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

